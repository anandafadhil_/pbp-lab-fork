## 1. Perbedaan XML dan JSON
JSON merupakan format penyimpanan data yang lebih mudah dan sederhana karena mudah untuk dipahami orang awam. XML pun tidak terlalu sulit untuk seorang pemula memahami alur dari program, karena tag yang digunakan pada XML dapat dikostumisasi sesuai variabel yang diinginkan oleh programmer. Untuk perbedaan mendasar nya, terdapat beberapa contoh seperti : 
- JSON memiliki base JavaScript sementara XML berasal dari MarkupLanguage
- JSON tidak memerlukan tag apapun
- JSON lebih mudah untuk merepresentasikan suatu object, sementara XML membutuhkan tag dan stuktur yang lebih kompleks
- dan lain-lain
## 2. Perbedaan XML dan HTML
Perbedaan yang terlihat jelas antara XML dan HTML adalah tipe variabel/bahasa yang digunakan. Hal ini disebabkan pada XML tidak memiliki code yang pasti dan lebih fleksibel bagi programmer untuk digunakan. Selain itu, XML merupakan markup language yang mendefinisikan markup language lainnya. Sementara itu hal lain yang berbeda tetapi tidak terlalu berdampak besar contohnya adalah : 
- case sensitive pada XML
- Null value pada HTML, dan lain sebagainya.
- HTML dapat memodifikasi tag yang ingin digunakan

# Sumber :
https://www.upgrad.com/blog/html-vs-xml/<br>
https://www.geeksforgeeks.org/difference-between-json-and-xml/<br>