import 'package:flutter/material.dart';
import 'package:lab_7/components/assignment_card.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          color: Color(0xFF313E58),
        ),
        scaffoldBackgroundColor: const Color(0xFF5965A4),
        fontFamily: 'Georgia',
        textTheme: const TextTheme(
          headline6: TextStyle(
              fontSize: 30.0, fontFamily: 'Arial', fontWeight: FontWeight.bold),
          bodyText2: TextStyle(
              fontSize: 20.0, fontFamily: 'Arial', color: Colors.white),
          bodyText1: TextStyle(
              fontSize: 18.0,
              fontFamily: 'Arial',
              color: Colors.black,
              fontStyle: FontStyle.italic),
        ),
      ),
      home: const MyHomePage(title: 'Deadline Table'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const assignmentData = <Map<String, String>>[
    {
      "title": "Struktur Data dan Algoritma",
      "subtitle": "Assignment: TP2\nPriority: PENTING\nDeadline: Nov 14, 2021",
    },
    {
      "title": "Kalkulus",
      "subtitle":
      "Assignment: Tugas 1\nPriority: PENTING\nDeadline: Dec 1, 2021",
    },
    {
      "title": "Kalkulus",
      "subtitle":
      "Assignment: Tugas 1\nPriority: PENTING\nDeadline: Dec 1, 2021",
    },
    {
      "title": "Kalkulus",
      "subtitle":
      "Assignment: Tugas 1\nPriority: PENTING\nDeadline: Dec 1, 2021",
    },
    {
      "title": "Kalkulus",
      "subtitle":
      "Assignment: Tugas 1\nPriority: PENTING\nDeadline: Dec 1, 2021",
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
          child: ListView(
            // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: [
                const SizedBox(
                  height: 80.0,
                  child: DrawerHeader(
                      child: Center(
                          child: Text('Reminder Bar',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25.0,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.normal))),
                      decoration: BoxDecoration(color: Color(0xFF313E58)),
                      margin: EdgeInsets.all(0.0),
                      padding: EdgeInsets.all(0.0)),
                ),
                ListTile(
                  title: const Text('Add Table'),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.push(
                      context,
                      MaterialPageRoute<void>(
                        builder: (BuildContext context) {
                          return Scaffold(
                            appBar: AppBar(
                              title: const Text('Add Table',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25.0,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold,
                                      fontStyle: FontStyle.normal)),
                            ),
                            body: const AddTable(),
                          );
                        },
                      ),
                    );
                  },
                ),
              ])),
      body: Center(
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          children: <Widget>[
            ...assignmentData.map((item) {
              return Padding(
                  child: AssignmentCard(
                      title: item["title"] ?? "",
                      subtitle: item["subtitle"] ?? ""),
                  padding: EdgeInsets.all(16));
            })
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute<void>(
              builder: (BuildContext context) {
                return Scaffold(
                  appBar: AppBar(
                    title: const Text('Add Table',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.normal)),
                  ),
                  body: const AddTable(),
                );
              },
            ),
          );
        },
        child: const Icon(Icons.add),
        backgroundColor: Colors.blue[800],
      ),
    );
  }
}

class ScrollableWidget extends StatelessWidget {
  //SCROLLABLE
  final Widget child;

  const ScrollableWidget(
      Container container,
      SizedBox sizedBox, {
        Key? key,
        required this.child,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.vertical,
        child: child,
      ),
    );
  }
}

class AddTable extends StatefulWidget {
  const AddTable({Key? key}) : super(key: key);

  @override
  AddTableSt createState() {
    return AddTableSt();
  }
}

class AddTableSt extends State<AddTable> {
  final _formKey = GlobalKey<FormState>();

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Color(0x04AA6DFF);
    }
    return Color(0x04AA6DFF);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(24),
        child: Card(
            child: Container(
                padding: EdgeInsets.all(24),
                color: Color(0xFF313E58),
                height: 300.0,
                child: Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      TextFormField(
                        decoration: const InputDecoration(
                            hintStyle: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Arial',
                                color: Colors.white),
                            hintText: 'Subject\'s name'),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: 'Arial',
                            color: Colors.white),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Mohon diisi';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            hintStyle: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Arial',
                                color: Colors.white),
                            hintText: 'Assignment\'s name'),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: 'Arial',
                            color: Colors.white),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Mohon diisi';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            hintStyle: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Arial',
                                color: Colors.white),
                            hintText: 'YYYY-MM-DD'),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: 'Arial',
                            color: Colors.white),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            hintStyle: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Arial',
                                color: Colors.white),
                            hintText: 'Priority list'),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: 'Arial',
                            color: Colors.white),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Column(
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  // Validate returns true if the form is valid, or false otherwise.
                                  if (_formKey.currentState!.validate()) {
                                    // If the form is valid, display a snackbar. In the real world,
                                    // you'd often call a server or save the information in a database.
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(content: Text('Processing Data')),
                                    );
                                  }
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                  MaterialStateProperty.resolveWith(
                                      getColor),

                                ),
                                child: const Text('Submit',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                        fontFamily: 'Arial',
                                        fontStyle: FontStyle.normal)),
                              ),
                            ],
                          )),
                    ],
                  ),
                ))));
  }
}
