from django.urls import path
from .views import friend_list, index

urlpatterns = [
    path('', index, name='index'), #localhost:8000/lab-1/
    path('friends', friend_list, name='friend_list') #localhost:8000/lab-1/friends/

    # TODO Add friends path using friend_list Views
]
